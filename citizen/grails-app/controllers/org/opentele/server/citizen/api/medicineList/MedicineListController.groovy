package org.opentele.server.citizen.api.medicineList

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.core.model.types.PermissionName

@Secured(PermissionName.NONE)
class MedicineListController {

    def medicineListService

    @Secured(PermissionName.PATIENT_LOGIN)
    def summary() {
        def document = medicineListService.getForPatient()
        if (!document) {
            return buildNotFoundResponse('medicineListSummary', 'Medicine list summary')
        }


        def body = [
                'uploadDate': document.uploadDate,
                'isNew': !document.isRead,
                'links': [
                        'self': createLink(mapping: 'medicineListSummary', absolute: true),
                        'medicineList': createLink(mapping: 'medicineList', absolute: true)
                ]
        ]

        [resource: body, resourceType: 'medicineListSummary']
    }

    @Secured(PermissionName.PATIENT_LOGIN)
    def show() {
        def document = medicineListService.getForPatient()
        if (!document) {
            return buildNotFoundResponse('medicineList', 'Medicine list')
        }

        medicineListService.markAsRead(document)

        response.setContentType(document.fileType.value)
        OutputStream out = response.getOutputStream()
        out << document.content
        out.flush()
    }

    private def buildNotFoundResponse(resourceType, resourceTypePretty) {
        return [status: 404, resourceType: 'errors', resource: [
                'message': "$resourceTypePretty not found",
                'errors': [
                        ['resource': resourceType,
                         'field': null,
                         'code': 'missing']]
        ]]
    }
}
