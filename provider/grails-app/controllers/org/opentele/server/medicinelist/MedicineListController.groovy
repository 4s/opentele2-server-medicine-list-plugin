package org.opentele.server.medicinelist

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import org.ghost4j.converter.PDFConverter
import org.ghost4j.converter.PSConverter
import org.ghost4j.document.PDFDocument
import org.ghost4j.document.PSDocument
import org.ghost4j.document.PaperSize
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.model.Document
import org.opentele.server.model.DocumentCategory
import org.opentele.server.model.FileType
import org.opentele.server.model.Patient
import org.springframework.beans.factory.annotation.Value

@Secured(PermissionName.NONE)
class MedicineListController {

    static allowedMethods = [show: 'GET', upload: 'POST']

    @Value('${medicineList.convertDocuments:false}')
    boolean shouldConvertPdf

    @Secured(PermissionName.MEDICINE_LIST_UPLOAD)
    def show() {

        def document = documentForPatient(params.long('id'))

        [document: document, patientId: params.long('id')]
    }

    private documentForPatient(long patientId) {
        def patient = Patient.get(patientId)
        def document = Document.findByPatientAndCategory(patient, DocumentCategory.MEDICINE_LIST)

        if (!document) {
            document = new Document(patient: patient, category: DocumentCategory.MEDICINE_LIST)
        }

        return document
    }

    @Secured(PermissionName.MEDICINE_LIST_UPLOAD)
    def open() {
        def document = Document.get(params.long('id'));

        response.setHeader "Content-disposition", "inline; filename=${document.filename}"
        response.setContentType(document.fileType.value)
        OutputStream out = response.getOutputStream()
        out << document.content
        out.flush()
    }

    @Transactional
    @Secured(PermissionName.MEDICINE_LIST_UPLOAD)
    def upload() {
        def file = request.getFile('file')

        if (!file) {
            flash.message = message(code: 'medicinelist.no.file.selected')
            redirect(action: "show", id: params.id)
            return
        }

        if (file.contentType != FileType.PDF.value) {
            flash.message = message(code: 'medicinelist.content.type.not.valid')
            redirect(action: "show", id: params.id)
            return
        }

        def pdfBytes = convertDocument(file.inputStream.bytes)
        def document = documentForPatient(params.long('id'))
        updateDocument(document, file, pdfBytes)

        redirect(action: "show", id: document.patient.id)
    }

    // Work around for a problem where PDF annotations are rotated when displayed. KIH-1898
    private def convertDocument(byte[] uploaded) {
        if (!shouldConvertPdf) {
            return uploaded
        }

        try {
            synchronized(this.getClass()) {
                def postScriptBytes = convertPdfToPostScript(uploaded)
                return convertPostScriptToPdf(postScriptBytes)
            }
        } catch (Exception ex) {
            log.warn("Exception while trying to convert PDF document. Ensure Ghostscript has been properly installed and is available on the path", ex)
            return uploaded
        }
    }

    private def convertPdfToPostScript(byte[] uploaded) {
        def inputPdf = new PDFDocument()
        inputPdf.load(new ByteArrayInputStream(uploaded))

        def postScript = new ByteArrayOutputStream()
        def postScriptConverter = new PSConverter()
        postScriptConverter.paperSize = PaperSize.A4
        postScriptConverter.convert(inputPdf, postScript)

        return postScript.toByteArray()
    }

    private def convertPostScriptToPdf(byte[] postScriptBytes) {
        def inputPostScript = new PSDocument()
        inputPostScript.load(new ByteArrayInputStream(postScriptBytes))

        def pdfConverter = new PDFConverter()
        pdfConverter.setPDFSettings(PDFConverter.OPTION_PDFSETTINGS_PREPRESS);
        def outputPdf = new ByteArrayOutputStream()
        pdfConverter.convert(inputPostScript, outputPdf)

        return outputPdf.toByteArray()
    }

    private updateDocument(Document document, file, pdfBytes) {
        document.fileType = FileType.fromContentType(file.contentType)
        document.filename = file.originalFilename
        document.content = pdfBytes
        document.isRead = false
        document.uploadDate = new Date()

        document.save()
    }
}
