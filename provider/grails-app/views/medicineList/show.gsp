
<%@ page import="org.opentele.server.model.Document" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.show.label" args="[g.message(code:'default.patient.medicinelist.label')]" /></title>
</head>
<body>


<div id="upload-medicineList" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[g.message(code:'default.patient.medicinelist.label')]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:uploadForm>
        <ol class="property-list medicineList">

            <li class="fieldcontain">
                <span id="upload-date-label" class="property-label"><g:message code="medicinelist.upload.date" /></span>
                <span class="property-value" aria-labelledby="upload-date-label">
                    <g:formatDate type="both" style="SHORT" date="${document.uploadDate}" />
                </span>
            </li>
            <li class="fieldcontain">
                <span id="filename-label" class="property-label"><g:message code="medicinelist.filename" /></span>
                <span class="property-value" aria-labelledby="filename-label">
                    <g:link action="open" id="${document.id}">${document.filename}</g:link>
                </span>
            </li>
            <li class="fieldcontain">
                <span id="upload-label" class="property-label"><g:message code="medicinelist.choose.file" /></span>
                <span class="property-value" aria-labelledby="upload-label">
                    <input class="upload-button" type="file" name="file" value="${message(code: 'medicinelist.button.file.label')}"/>
                </span>
            </li>
        </ol>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${patientId}" />
            <g:actionSubmit class="save" action="upload" value="${message(code: 'medicinelist.button.upload.label')}" />
        </fieldset>
    </g:uploadForm>
</div>
</body>
</html>
